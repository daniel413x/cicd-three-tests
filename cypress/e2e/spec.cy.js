/* eslint-disable no-undef */

describe('point plotter', () => {
  beforeEach(() => {
    // cy.request('POST', `${process.env.API_URL}/api/testing/reset`);
    // cy.visit(process.env.CLIENT_URL);
    cy.visit(Cypress.env('CLIENT_URL'));
  });
  it('should fetch the coordinate data', () => {
    cy.intercept('GET', `${Cypress.env('API_URL')}/data/coords`).as('getData');
    cy.get('button[data-cy="polygon-hull-btn"]')
      .click();
    cy.wait('@getData').its('response.statusCode').should('eq', 200);
  });
  it('should fetch the bar graph data', () => {
    cy.intercept('GET', `${Cypress.env('API_URL')}/data/numbers?size=5`).as('getData');
    cy.get('button[data-cy="bar-chart-btn"]')
      .click();
    cy.wait('@getData').its('response.statusCode').should('eq', 200);
  });
});
